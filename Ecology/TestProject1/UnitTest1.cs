using NUnit.Framework;
using ClassLibrary;

namespace TestProject1
{
    public class Tests
    {
        Class1 class1 = new Class1();
        [SetUp]
        public void Setup()
        {
        }
        const double B = 133.7;
        const double M = 42.85;
        const double GM = 0.205;
        const double QNRM = 40190;
        const double QNRG = 33410;
        const double ALFA = 1.345;
        const double O2 = 28.25;
        const double KP = 0.81;
        const double VK = 1500;
        const double VKB = 3500;
        const double VBB = 47187;
        const double NR = 0.5;
        const double VKV = 0;
        const double N2K = 2.85;
        const double FG = 107.4;

        [Test]
        public void Test1()
        {
            const double expected = 2296.474;
            class1.Azot(B, M, GM, QNRM, QNRG, ALFA, O2, KP, VK, VKB, VBB, NR, VKV, N2K, FG);
            double test = class1.TB;
            Assert.AreEqual(expected, Math.Round(test, 3));
        }
        [Test]
        public void Test2()
        {
            const double expected = 2005.587299;
            class1.Azot(B, M, GM, QNRM, QNRG, ALFA, O2, KP, VK, VKB, VBB, NR, VKV, N2K, FG);
            double test = class1.T;
            Assert.AreEqual(expected, Math.Round(test, 6));
        }
        [Test]
        public void Test3()
        {
            const double expected = 66.261616;
            class1.Azot(B, M, GM, QNRM, QNRG, ALFA, O2, KP, VK, VKB, VBB, NR, VKV, N2K, FG);
            double test = class1.N2;
            Assert.AreEqual(expected, Math.Round(test, 6));
        }
        [Test]
        public void Test4()
        {
            const double expected = 5.053573;
            class1.Azot(B, M, GM, QNRM, QNRG, ALFA, O2, KP, VK, VKB, VBB, NR, VKV, N2K, FG);
            double test = class1.O2I;
            Assert.AreEqual(expected, Math.Round(test, 6));
        }
        [Test]
        public void Test5()
        {
            const double expected = 0.009311;
            class1.Azot(B, M, GM, QNRM, QNRG, ALFA, O2, KP, VK, VKB, VBB, NR, VKV, N2K, FG);
            double test = class1.NOR;
            Assert.AreEqual(expected, Math.Round(test, 6));
        }
        [Test]
        public void Test6()
        {
            const double expected = 0.009311;
            class1.Azot(B, M, GM, QNRM, QNRG, ALFA, O2, KP, VK, VKB, VBB, NR, VKV, N2K, FG);
            double test = class1.NO1;
            Assert.AreEqual(expected, Math.Round(test, 6));
        }
        [Test]
        public void Test7()
        {
            const double expected = 14.588225;
            class1.Azot(B, M, GM, QNRM, QNRG, ALFA, O2, KP, VK, VKB, VBB, NR, VKV, N2K, FG);
            double test = class1.VALFA;
            Assert.AreEqual(expected, Math.Round(test, 6));
        }
        [Test]
        public void Test8()
        {
            const double expected = 6.846687;
            class1.Azot(B, M, GM, QNRM, QNRG, ALFA, O2, KP, VK, VKB, VBB, NR, VKV, N2K, FG);
            double test = class1.VNO;
            Assert.AreEqual(expected, Math.Round(test, 6));
        }
        [Test]
        public void Test9()
        {
            const double expected = 9.17593;
            class1.Azot(B, M, GM, QNRM, QNRG, ALFA, O2, KP, VK, VKB, VBB, NR, VKV, N2K, FG);
            double test = class1.MNO;
            Assert.AreEqual(expected, Math.Round(test, 6));
        }
        [Test]
        public void Test10()
        {
            const double expected = 0.214141;
            class1.Azot(B, M, GM, QNRM, QNRG, ALFA, O2, KP, VK, VKB, VBB, NR, VKV, N2K, FG);
            double test = class1.MYNO;
            Assert.AreEqual(expected, Math.Round(test, 6));
        }
        [Test]
        public void Test11()
        {
            const double expected = 0.000299;
            class1.Azot(B, M, GM, QNRM, QNRG, ALFA, O2, KP, VK, VKB, VBB, NR, VKV, N2K, FG);
            double test = class1.MCNO;
            Assert.AreEqual(expected, Math.Round(test, 6));
        }
        [Test]
        public void Test12()
        {
            const double expected = 0.00013;
            class1.Azot(B, M, GM, QNRM, QNRG, ALFA, O2, KP, VK, VKB, VBB, NR, VKV, N2K, FG);
            double test = class1.NO;
            Assert.AreEqual(expected, Math.Round(test, 6));
        }

    }
}