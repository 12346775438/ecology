﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlTypes;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ClassLibrary;
using Microsoft.Office.Interop.Excel;
using Microsoft.Reporting.WinForms;
using Newtonsoft.Json.Linq;
using static System.Windows.Forms.VisualStyles.VisualStyleElement;

namespace WindowsFormsApp1
{
    public partial class Main : Form
    {
        public Main()
        {
            InitializeComponent();
        }
        private BindingSource bindingSource;

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                Class1 Class1 = new Class1();
                Class1.Azot(Convert.ToDouble(textBox1.Text), Convert.ToDouble(textBox2.Text), Convert.ToDouble(textBox3.Text), Convert.ToDouble(textBox4.Text), Convert.ToDouble(textBox5.Text), Convert.ToDouble(textBox6.Text), Convert.ToDouble(textBox7.Text), Convert.ToDouble(textBox8.Text), Convert.ToDouble(textBox9.Text), Convert.ToDouble(textBox10.Text), Convert.ToDouble(textBox11.Text), Convert.ToDouble(textBox12.Text), Convert.ToDouble(textBox13.Text), Convert.ToDouble(textBox14.Text), Convert.ToDouble(textBox15.Text));
                richTextBox1.Text = $"TB = {Math.Round(Class1.TB, 3)} К" +
                    $"\nT= {Math.Round(Class1.T, 6)} К" +
                    $"\nN2 = {Math.Round(Class1.N2, 6)} %" +
                    $"\nO2I = {Math.Round(Class1.O2I, 6)} %" +
                    $"\nNO1 = {Math.Round(Class1.NO1, 6)} К" +
                    $"\nNOR = {Math.Round(Class1.NOR, 6)} %" +
                    $"\nVALFA = {Math.Round(Class1.VALFA, 6)} м^3/c" +
                    $"\nVNO = {Math.Round(Class1.VNO, 6)} м^3/ч" +
                    $"\nMNO = {Math.Round(Class1.MNO, 6)} кг/ч" +
                    $"\nMYNO = {Math.Round(Class1.MYNO, 6)} кг/т" +
                    $"\nMCNO = {Math.Round(Class1.MCNO, 6)} кг/м^3" +
                    $"\nNO = {Math.Round(Class1.NO, 6)} %";
                bindingSource = new BindingSource();
                bindingSource.DataSource = Class1;
                ReportDataSource reportDataSource = new ReportDataSource("DataSet1", bindingSource);
                reportViewer1.LocalReport.ReportPath = @"Report.rdlc";
                reportViewer1.LocalReport.DataSources.Clear();
                reportViewer1.LocalReport.DataSources.Add(reportDataSource);
                reportViewer1.RefreshReport();
            }
            catch (Exception ex)
            {
                MessageBox.Show($"Ошибка: {ex.Message}", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }
        private void button2_Click(object sender, EventArgs e)
        {
            Main Main = new Main();
            Main.Show();
        }
        private void button5_Click(object sender, EventArgs e)
        {
            openFileDialog1.Filter = "Config Files (*.cfg)|*.cfg|txt files (*.txt)|*.txt|All Files (*.*)|*.*";
            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                try
                {
                    string filePath = openFileDialog1.FileName;
                    string[] lines = File.ReadAllLines(filePath);
                    textBox1.Text = lines[0];
                    textBox2.Text = lines[1];
                    textBox3.Text = lines[2];
                    textBox4.Text = lines[3];
                    textBox5.Text = lines[4];
                    textBox6.Text = lines[5];
                    textBox7.Text = lines[6];
                    textBox8.Text = lines[7];
                    textBox9.Text = lines[8];
                    textBox10.Text = lines[9];
                    textBox11.Text = lines[10];
                    textBox12.Text = lines[11];
                    textBox13.Text = lines[12];
                    textBox14.Text = lines[13];
                    textBox15.Text = lines[14];
                }
                catch (Exception ex)
                {
                    MessageBox.Show($"Ошибка при чтении файла: {ex.Message}", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            saveFileDialog1.Filter = "Config Files (*.cfg)|*.cfg|txt files (*.txt)|*.txt|All Files (*.*)|*.*";
            if (saveFileDialog1.ShowDialog() == DialogResult.OK)
            {
                string filePath = saveFileDialog1.FileName;
                try
                {
                    using (StreamWriter writer = new StreamWriter(filePath))
                    {
                        writer.WriteLine(textBox1.Text);
                        writer.WriteLine(textBox2.Text);
                        writer.WriteLine(textBox3.Text);
                        writer.WriteLine(textBox4.Text);
                        writer.WriteLine(textBox5.Text);
                        writer.WriteLine(textBox6.Text);
                        writer.WriteLine(textBox7.Text);
                        writer.WriteLine(textBox8.Text);
                        writer.WriteLine(textBox9.Text);
                        writer.WriteLine(textBox10.Text);
                        writer.WriteLine(textBox11.Text);
                        writer.WriteLine(textBox12.Text);
                        writer.WriteLine(textBox13.Text);
                        writer.WriteLine(textBox14.Text);
                        writer.WriteLine(textBox15.Text);
                    }
                    MessageBox.Show("Конфигурация сохранена успешно", "Успех", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                catch (Exception ex)
                {
                    MessageBox.Show($"Ошибка при сохранении файла: {ex.Message}", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }
        private void Main_Load(object sender, EventArgs e)
        {

            this.reportViewer1.RefreshReport();
        }
    }
}

