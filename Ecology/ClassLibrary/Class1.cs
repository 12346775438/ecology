﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassLibrary
{
    public class Class1
    {
        public double TB { get; set; }
        public double T { get; set; }
        public double N2 { get; set; }
        public double O2I { get; set; }
        public double NOR { get; set; }
        public double NO1 { get; set; }
        public double VNO { get; set; }
        public double VALFA { get; set; }
        public double MNO { get; set; }
        public double NO { get; set; }
        public double MYNO { get; set; }
        public double MCNO { get; set; }

        public void Azot(double B, double M, double GM, double QNRM, double QNRG, double ALFA,
        double O2, double KP, double VK, double VKB, double VBB, double NR, double VKV, double N2K, double FG)
        {
            TB = (2440 / (Math.Pow(ALFA, 0.27)) * Math.Exp((0.135 - 0.03 * ALFA) * GM));
            T = TB - 2 * (TB - TB * KP) / 3;
            N2 = 84.943 - (0.9159 * O2) + (3.2501 * ALFA) + (2.0977 * ALFA);
            O2I = Math.Log(ALFA) * ((1.84 - ALFA) * GM + (23.69 - 5.3 * ALFA)) + (6.078 * GM * (Math.Pow(1.0048, (TB * KP - 273))) + 65.8 * (Math.Pow(1.0047, (TB * KP - 273)))) * Math.Pow(10, -6);
            double X1 = Math.Exp(-67500 / T);
            double X2 = Math.Exp(-46000 / T);
            double TT = KP * TB;
            double o = Math.Sqrt(Math.Pow(T - TT, 2)) / TT;
            NOR = Math.Sqrt(N2 * O2I) * Math.Exp(-21500 / (TB / KP));
            NO1 = NOR * (1 + (Math.Pow(o, 2) / 2) * (0.5 * (Math.Pow(X1, 2) - Math.Pow(X2, 2)) + (X1 - X2)));
            double VMA = (217.47 * ALFA - 52.3) * Math.Pow(O2, -0.88 * Math.Pow(ALFA, 0.11));
            double VRA = (182 * ALFA - 56) * Math.Pow(O2, -0.82 * Math.Pow(ALFA, 0.16));
            VALFA = (B * M * 29310) / 3600 * (GM * VMA / QNRM + (1 - GM) * VRA / QNRG);
            double pNO = 1.3402;
            double pN = 1.2505;
            double W1NO = 0.01 * VALFA * NO1;
            double WpNO = 0.01 * NOR * VALFA;
            double M1NO = W1NO * pNO;
            double MpNO = WpNO * pNO;
            double C1NO = 0.01 * NO1 * pNO;
            double CpNO = MpNO / VALFA;
            double m1NO = (0.01 * VALFA * NO1 * pNO) / M;
            double mpNO = MpNO / M;
            double nMT = QNRM / 29310;
            double mM = (B * M * GM) / nMT;
            double MNN = 0.01 * NR * B * M * GM / nMT;
            double M2NO = 2.1422 * MNN;
            double W2NO = M2NO / pNO;
            double C2NO = M2NO / (3600 * VALFA);
            double m2NO = M2NO / M;
            double C3NO = (0.021422 * VKV * 0.5 * pN) / (3600 * VALFA);
            double M3NO = 0.02142 * VKV * 0.5;
            double m3NO = M3NO / M;
            double W3NO = 0.02142 * VKV * 0.5;
            VNO = W1NO + W2NO + W3NO;
            MNO = M1NO + M2NO + M3NO;
            MYNO = m1NO + m2NO + m3NO;
            MCNO = C1NO + C2NO + C3NO;
            NO = VNO / (3600 * VALFA);
        }

    
    }
}
